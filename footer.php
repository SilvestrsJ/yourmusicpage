<footer class="bg-light">
        <div class="container py-3 py-sm">
            <div class="row">

                <div class="col-12 col-sm-4 col-lg-4 d-flex flex-column align-items-center">
                    <h5 class="name-text-style">Navigation links</h5>
                    <ul class="list-unstyled">
                        <li class="mb-1 text-style"><a href="#section-description" class="text-decoration-none">Description</a></li>
                        <li class="mb-1 text-style"><a href="#section-features" class="text-decoration-none">Features</a></li>
                        <li class="mb-1 text-style"><a href="#section-pricing" class="text-decoration-none">Pricing</a></li>
                        <li class="mb-1 text-style"><a href="#section-styles" class="text-decoration-none">Styles</a></li>
                    </ul>
                </div>

                <div class="col-12 col-sm-4 col-lg-4 d-flex flex-column align-items-center">
                    <h5 class="name-text-style">Follow us</h5>
                    <ul class="list-unstyled">
                        <li class="mb-2 text-style"><a href="https://www.facebook.com/" class="text-decoration-none"><i class="fab fa-facebook-f fa-lg mr-2"></i>Facebook</a></li>
                        <li class="mb-2 text-style"><a href="https://www.instagram.com/" class="text-decoration-none"><i class="fab fa-instagram fa-lg mr-2"></i>Instagram</a></li>
                        <li class="mb-2 text-style"><a href="https://www.youtube.com/" class="text-decoration-none"><i class="fab fa-youtube fa-lg mr-2"></i>Youtube</a></li>
                        <li class="mb-2 text-style"><a href="https://www.spotify.com/" class="text-decoration-none"><i class="fab fa-spotify fa-lg mr-2"></i>Spotify</a></li>
                        <li class="mb-2 text-style"><a href="https://www.telegram.com/" class="text-decoration-none"><i class="fab fa-telegram fa-lg mr-2"></i>Telegram</a></li>
                    </ul>
                </div>

                <div class="col-12 col-sm-4 col-lg-4 d-flex flex-column align-items-center">
                    <h5 class="name-text-style">Our location</h5>
                    <adress class="text-center"><strong class="text-center">Your.Music.Page</strong><br>
                        <p class="text-style text-center">Brivibas iela 78, Riga, Latvia</p>

                        <abbr title="telephone">+371 2303 3233</abbr><a href="tel: +2332 3223"></a><br>
                        <abbr title="email">yourmusicpage@ymp.com</abbr><a href="email: ymp@gmail.com"></a><br>
                    </adress>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <ul class="list-inline text-center border-top mb-4">
                        <li class="list-inline-item mt-4">© 2020 YMP.</li>
                        <li class="list-inline-item">All rights reserved.</li>
                        <li class="list-inline-item"><a href="#" data-toggle="modal" dara-target="#modal">Temrs of the use and privacy politic</a></li>
                    </ul>
                </div>
            </div>
        </div>
</footer>
