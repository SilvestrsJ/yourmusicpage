<?php
    session_start();
    if(isset($_SESSION['userlogin'])){
        header("Location: index.php");
    }else{
        // $error = "OOOPS Try again. Something wrong.";
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/loginstyle.css">
        <link rel="shortcut icon" type="icon" href="./img/icon.png">
        <title>Your.Music.Page</title>
    </head>
    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="user-card">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                        <i class="fab fa-napster fa-6x mt-5 mb-5"></i>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center form-container">
                        <form>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text "><i class="fas fa-user"></i></span>
                                </div>
                                </p>
                                <input type="text" name="email" id="email" placeholder="Email" class="form-control input-user" required>
                            </div>

                            <div class="input-group mb-2">
                                <div class="input-group-append">
                                    <span class="input-group-text "><i class="fas fa-key"></i></span>
                                </div>

                                <input type="password" name="password" id="password" placeholder="Password" class="form-control input-pass" required>
                            </div>
            
                            <div class="d-flex justify-content-center mt-1 login-container">
                                <button type="button" name="button" id="login" class="btn login_btn btn-success mt-4">Login</button>
                            </div>
                        </form>
                    </div>

                    <div class="mt-4">
                        <div class="d-flex justify-content-center links">
                            Don't have account?? <a href="registration.php" class="ml-2"> Sign in?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <script type="text/javascript" scr="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        
        <script>
            $(function(){
                $('#login').click(function(e){

                    var valid = this.form.checkValidity();

                    if(valid){
                        var email = $('#email').val();
                        var password = $('#password').val();
                    }

                    e.preventDefault();

                    $.ajax({
                        type: 'POST',
                        url: 'jslogin.php',
                        data: {email: email, password: password},

                        success: function(data){
                            window.location.href = "index.php"
                        },

                        error: function(data){
                            alert('there are while error do the opperation');
                        }

                    });
                });
            });
        </script>
    </body>
</html>