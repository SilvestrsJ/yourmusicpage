

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M" crossorigin="anonymous">
   
    <link rel="stylesheet" href="./css/style.css">
    
    <title>Your.Music.Page</title>

  </head>
  <body>


    
    <!--JUMBOTRON-->
    <section id="section-top" class="jumbotron jumbotron-fluid d-flex justify-content-center align-items-center">
      <div class="container text-center top-text-style">
        <div class="yrp-box">
          <h1 class="display-1 text-primery text-uppercase text-warning this-text-style-top my-hover">Your.Music.Page</h1>
        </div>
        <p class="display-4 music-is-everything">Music is everything</p>
        <p class="free-beat-text mt-2"><strong>Need a beat ? Get beat for absolutely free!</strong></p>
        <a href="https://failiem.lv/down.php?i=pkmad6xp" class="btn btn-lg btn-warning mt-2"><i class="fas fa-arrow-alt-circle-right mr-2"></i>Dowload there</a>
      </div>
    </section>

    <div class="container">

      <!-- DESCRIPTION -->
      <section id="section-description">
        <h2 class="display-4 text-center name-text-style">Music making is in our blood</h2>
        <p class="mt-3 text-style text-center">Make some noise, explore a new sound, create a song or collaborate with others. Welcome to the collective of passionate creators, whether you already are one or aspire to become one. Music is a form of art - an expression of emotions through harmonic frequencies. It’s almost impossible to recognize music that has been created on a home PC. In fact, computers are viewed today as a must have instrument for musicians that needs to be learned and mastered. Songs created digitally are often unable to be distinguished from works created using traditional analog methods. That's why musicians, instruments, and their relationship is the focus– the only difference is that composing on a PC now makes it possible for everyone to express themselves musically. Some basic ideas are helpful to know beforehand, however these can be learned in less time than it takes to learn how to read notes. Information about how to organize music, to be able to find music files quickly, and also which audio codec (e.g. MP3) is useful but can also be learned intuitively. Knowing how to work with loops and MIDI, since these form the basic building blocks for working with sounds in a sequenzer, can also be helpful but are not necessary as they can learned through practice. In fact, with Music Maker, users will have the ability to learn about music production while having fun and trying out the program.

      </section>

      <!--FEATURES -->
      <section id="section-features">
        <h2 class="display-4 text-center name-text-style">Features</h2>
          <div class="row mt-4 text-center d-flex justify-content-center">
            <div class="col-12 col-sm-6 col-lg-4 d-flex flex-column align-items-center mb-5">
              <div class="comment more text-style">
                Musical Futures is an approach to the teaching and learning of music that promotes innovation, inclusion, diversity and lots and lots of music-making which is driven by the musical culture of the participants, rather than being limited to a specific musical style or genre. ​We provide teachers from primary schools to tertiary teacher education institutions with training, support, networks and resources to deliver practical, engaging, developmental, pedagogically-grounded music programs in the classroom. Musical Futures is sustainable in that it empowers and supports music teachers and is transferable to a range of learning contexts, demonstrated in the way it has grown from a small pilot project with 14-year-olds in the UK to a worldwide, globally recognised approach to music learning and teaching. </p>
              </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-4 d-flex flex-column align-items-center mb-5">
              <div class="comment more text-style">
                ​We provide teachers from primary schools to tertiary teacher education institutions with training, support, networks and resources to deliver practical, engaging, developmental, pedagogically-grounded music programs in the classroom. Musical Futures is sustainable in that it empowers and supports music teachers and is transferable to a range of learning contexts, demonstrated in the way it has grown from a small pilot project with 14-year-olds in the UK to a worldwide, globally recognised approach to music learning and teaching. Musical Futures is an approach to the teaching and learning of music that promotes innovation, inclusion, diversity and lots and lots of music-making which is driven by the musical culture of the participants, rather than being limited to a specific musical style or genre. ​We provide teachers from primary schools to tertiary teacher education institutions with training, support, networks and resources to deliver practical, engaging, developmental, pedagogically-grounded music programs in the classroom.
              </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-4 d-flex flex-column align-items-center mb-5">
              <div class="comment more text-style">
                Musical Futures is sustainable in that it empowers and supports music teachers and is transferable to a range of learning contexts, demonstrated in the way it has grown from a small pilot project with 14-year-olds in the UK to a worldwide, globally recognised approach to music learning and teaching. Musical Futures is an approach to the teaching and learning of music that promotes innovation, inclusion, diversity and lots and lots of music-making which is driven by the musical culture of the participants, rather than being limited to a specific musical style or genre. 
              </div>
            </div>
          </div>
      </section>

      <!--CAROUSEL-->
      <section class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>

          <div class="carousel-inner">
              <div class="carousel-item active">
              <img class="d-block w-100" src="./img/carousel-1.jpg" alt="First slide">
              <div class="carousel-caption d-none d-md-block">
                <h3 class="name-text-style">Mix Sounds</h3>
                <p class="text-style">Get fancy</p>
              </div>
          </div>

          <div class="carousel-item">
            <img class="d-block w-100" src="./img/carousel-2.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h3 class="name-text-style">Chop Samples</h3>
                <p class="text-style">Chop samples like a boss</p>
            </div>
          </div>

          <div class="carousel-item">
            <img class="d-block w-100" src="./img/carousel-3.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
              <h3 class="name-text-style">Live Instruments</h3>
              <p class="text-style">Feel real Atmosphere</p>
            </div>
          </div>
        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only name-text-style">Previous</span>
        </a>

        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only name-text-style">Next</span>
        </a>
      </section>

    <!--CHOOSE YOUR PLAN -->
        <section id="section-pricing">
          <h2 class="display-4 text-center mb-4 name-text-style">Choose your plan</h2>
          <div class="row">
            <div class="col-12 col-md-4 mb-4">
              <div class="card">
                <div class="card-header bg-success text-white text-center">
                    <h4 class="name-text-style-bold">Basic Lease</h4>
                    <h2 class="name-text-style-bold">$100</h2>
                    <p class="text-style-bold-fed">Instant Delivery</p>
                </div>

                <ul class="list-group">
                  <li class="list-group-item text-style">MP3 & WAV (Untagged)</li>
                  <li class="list-group-item text-style">Music Video</li>
                  <li class="list-group-item text-style">Non-profit Live Performances only</li>
                  <li class="list-group-item text-style">For iTunes, Spotify etc.</li>
                  <li class="list-group-item text-style">Distribution Copies 5 000</li>
                  <li class="list-group-item text-style">Radio Broadcasting rights (1 Station)</li>
                  <li class="list-group-item text-style">Online Audio Streams 100 000</li>
                  <li class="list-group-item text-style">Online Video Streams 100 000</li>
                  <li class="list-group-item text-style">No sole ownership</li>
                  <li class="list-group-item text-style">Credit must be OneS Beats</li>
                </ul>

                <div class="card-footer text-center">
                  <a href="#" class="btn btn-success btn-sm text-style-bold-fed">Read License</a>
                </div>
              </div>
            </div>  

            <div class="col-12 col-md-4 mb-4">
              <div class="card">
                <div class="card-header bg-warning text-white text-center">
                  <h4 class="name-text-style-bold">Pro Lease</h4>
                  <p class="mt-0 warning-expensive">Best Choice</p>
                  <h2 class="mt-0 name-text-style-bold">$500</h2>
                  <p class="text-style-bold-fed">Instant Delivery</p>
                </div>

                <ul class="list-group">
                  <li class="list-group-item text-style">MP3 & WAV (Untagged)</li>
                  <li class="list-group-item text-style">Music Video unlimited</li>
                  <li class="list-group-item text-style">Non-profit Live Performances only</li>
                  <li class="list-group-item text-style">For iTunes, Spotify etc.</li>
                  <li class="list-group-item text-style">Distribution Copies 10 000</li>
                  <li class="list-group-item text-style">Radio Broadcasting rights (3 Station)</li>
                  <li class="list-group-item text-style">Online Audio Streams 200 000</li>
                  <li class="list-group-item text-style">Online Video Streams 300 000</li>
                  <li class="list-group-item text-style">No sole ownership</li>
                  <li class="list-group-item text-style">Credit must be OneS Beats</li>
                </ul>

                <div class="card-footer text-center">
                  <a href="#" class="btn btn-success btn-sm text-style-bold-fed">Read License</a>
                </div>
              </div>
            </div>    

            <div class="col-12 col-md-4 mb-4">
              <div class="card">
                <div class="card-header bg-danger text-white text-center">
                  <h4 class="name-text-style-bold">Exclusive Rights</h4>
                  <h2 class="name-text-style-bold">$999</h2>
                  <p class="text-style-bold-fed">Instant Delivery</p>
                </div>

                <ul class="list-group">
                  <li class="list-group-item text-style">MP3 & WAV (Untagged)</li>
                  <li class="list-group-item text-style">Music Video unlimited</li>
                  <li class="list-group-item text-style">Non-profit Live Performances only</li>
                  <li class="list-group-item text-style">For iTunes, Spotify etc.</li>
                  <li class="list-group-item text-style">Distribution Copies unlimited</li>
                  <li class="list-group-item text-style">Radio Broadcasting rights (unlimited)</li>
                  <li class="list-group-item text-style">Online Audio Streams unlimited</li>
                  <li class="list-group-item text-style">Online Video Streams unlimited</li>
                  <li class="list-group-item text-style">No sole ownership</li>
                  <li class="list-group-item text-style">Credit must be OneS Beats</li>
                  <li class="list-group-item text-style">Beat is removed from sale</li>
                </ul>

                <div class="card-footer text-center">
                    <a href="#" class="btn btn-danger btn-sm center-btn text-style-bold-fed">Read License</a>
                </div>
              </div>
            </div>    
          </div>

          





        </section>
    </div>

    <!-- STYLE OF BEATS -->
    <div class="middle-background-img"></div>
    <div class="container">
      <section id="section-styles">
        <h2 class="display-4 text-center mb-4 name-text-style">Style of beats</h2> 
        <div class="row">
          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold ">Oldschool</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Trap</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Underground</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Jazz</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Lo-fi</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Boom bap</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Experimental</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Chill</h4>
              </div>
            </div>
          </div>

          <div class="col-12 col-md-4 mb-4">
            <div class="card box-shadow">
              <div class="card-header text-dark text-center">
                <h4 class="m-0 text-style-bold">Indie</h4>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    
    <!-- INCLUDE FOOTER -->
    <?php
      require_once('footer.php');
    ?>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/16.1.0/smooth-scroll.min.js"></script>

    <script>
      var prevScrollpos = window.pageYOffset;

          window.onscroll = function() {
          var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
          document.getElementById("nav").style.top = "0";
      } else {
          document.getElementById("nav").style.top = "-70px";
      }
          prevScrollpos = currentScrollPos;
      }

// show more/less
      $(document).ready(function() {
        var showChar = 255;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";

        $('.more').each(function() {
          var content = $(this).html();

          if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span> <span class="morecontent"><span style="display:none;">' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink less">' + moretext + '</a></span>';

            $(this).html(html);
          }
        });

        $(".morelink").click(function(){
          if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(lesstext);
          } else {
            $(this).addClass("less");
            $(this).html(moretext);
          }
          $(this).parent().prev().toggle();
          $(this).prev().toggle();
          return false;
        });
      });
    </script>
</body>
</html>

