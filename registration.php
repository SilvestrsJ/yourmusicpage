<?php
    require_once('config.php');

?>

<!DOCTYPE html>
    <html lang="en">
        
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="shortcut icon" type="icon" href="./img/icon.png">
        <title>Your.Music.Page</title>
    </head>
    
    <body>
        <div class="">
            <form action="login.php" method="post">
                <div class="container d-flex justify-content-center align-items-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="mt-5 music-is-everything">Registration</h1>
                            <hr class="mb-3">
                            <label for="firstname" class="text-style-bold-r">Firstname:</label>
                            <input class="form-control mb-4" type="text" id="firstname" name="firstname" placeholder="Firstname" required>

                            <label for="lastname" class="text-style-bold-r">Lastname:</label>
                            <input class="form-control mb-4" type="text" id="lastname" name="lastname" placeholder="Lastname" required>

                            <label for="email" class="text-style-bold-r">Email:</label>
                            <input class="form-control mb-4" type="email" id="email" name="email" placeholder="Email" required>

                            <label for="phonenumber" class="text-style-bold-r">Phonenumber:</label>
                            <input class="form-control mb-4" type="text" id="phonenumber" name="phonenumber" placeholder="Phonenumber" required>

                            <label for="password" class="text-style-bold-r">Password:</label>
                            <input class="form-control mb-4" type="password" id="password" name="password" placeholder="Password" required>

                            <hr class="mb-3">
                            <button type="submit" name="create" id="register"  value="Sign Up" class="btn btn-warning">Sign Up</button>

                        </div>
                    </div>
                </div>
            </form>

            <div class="mt-4">
                <div class="d-flex justify-content-center links">
                    <a href="login.php" class="ml-2"> Login?</a>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

        <script type="text/javascript">

            $(function(){
                $('#register').click(function(e){

                    var valid = this.form.checkValidity();

                    if(valid){
                        
                        var firstname = $('#firstname').val();
                        var lastname = $('#lastname').val();
                        var email = $('#email').val();
                        var phonenumber = $('#phonenumber').val();
                        var password = $('#password').val();

                        e.preventDefault();

                        $.ajax({
                            type: 'POST',
                            url: 'process.php',
                            data: {firstname: firstname, lastname: lastname, email: email, phonenumber: phonenumber, password: password},
                            success: function(data){
                                Swal.fire({
                                    'title' : 'Successful',
                                    'text' : data,
                                    'type' : 'Success'
                                })
                            },
                            error: function(data){
                                Swal.fire({
                                    'title' : 'Error',
                                    'text' : 'There were errors while saving the data.',
                                    'type' : 'error'
                                })
                            }
                        });
                    }else{

                    }
                });
            });
        </script>
    </body>
</html>